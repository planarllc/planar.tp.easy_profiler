
#You need to add next line in you pro-file
#DEFINES += BUILD_WITH_EASY_PROFILER

LIB_DIR=""

win32-g++:LIB_DIR="$$PWD/lib/mingw/"
win32-msvc2013*:LIB_DIR="$$PWD/lib/msvc2013/"
win32-msvc2015*:LIB_DIR="$$PWD/lib/msvc2015/"
unix:!macx:LIB_DIR="$$PWD/lib/linux/"

message("easy_profiler in " $$PWD "; libs in " $$LIB_DIR)

INCLUDEPATH += "$$PWD/include"

LIBS += -L$$LIB_DIR -leasy_profiler

#unix:!macx:profiler_libs.files = $$LIB_DIR/*.so
#win32:profiler_libs.files = $$LIB_DIR/*.dll

#INSTALLS += profiler_libs
